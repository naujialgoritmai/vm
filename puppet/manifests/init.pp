$mysql_password = "pass"

Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

exec { 'locale-gen en_US.UTF-8':
  path => '/usr/sbin',
}

exec { 'export LANG=en_US.UTF-8':
  path => '/usr/bin',
}

exec { 'sudo apt-get install software-properties-common -y':
  path => '/usr/bin',
}

exec { 'sudo add-apt-repository ppa:ondrej/php5-5.6 -y':
  path => '/usr/bin',
}

exec { 'apt-get update':
  path => '/usr/bin',
}

service { 'memcached':
  ensure => "running",
  enable => true,
}

package { 'curl':
  ensure => present,
}

package { 'htop':
  ensure => present,
}

package { 'mc':
  ensure => present,
}

package { 'git':
  ensure => present,
}

file { '/var/www/runtime':
  ensure => 'directory',
}

file { '/var/www/runtime/logs':
  ensure => 'directory',
}

include nginx, php, mysqlcreate, yii