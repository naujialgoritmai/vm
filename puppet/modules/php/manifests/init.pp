class php {

  # @todo add https://github.com/AOP-PHP/AOP
  # Install the php5-fpm and php5-cli packages
  package { ['php5-fpm',
             'php5-cli',
             'mcrypt',
             'php5-mcrypt',
             'php5-mysqlnd',
             'php5-gd',
             'php5-memcached',
             'php5-intl',
             'php5-curl',
             'libpcre3-dev',
             'libgmp-dev',
             'php5-gmp',
             'php5-xdebug']:
    ensure => present,
    require => Exec['apt-get update'],
  }

  # Make sure php5-fpm is running
  service { 'php5-fpm':
    ensure => running,
    require => Package['php5-fpm'],
  }

  exec{'enable mcrypt':
    command => "sudo php5enmod mcrypt && sudo service php5-fpm restart"
  }

}