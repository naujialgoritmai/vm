class yii {

    exec { "install composer":
        command => "curl -s http://getcomposer.org/installer | php",
        path    => [ "/usr/bin/", "/home/vagrant" ]
    }

    exec { "rename composer":
        command => "mv /home/vagrant/composer.phar /usr/local/bin/composer",
        path    => [ "/bin/"]
    }

}