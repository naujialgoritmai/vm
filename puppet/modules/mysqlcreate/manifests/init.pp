class mysqlcreate {

  class { '::mysql::client':}

  class { '::mysql::server':
    root_password => $mysql_password
  }

  mysql::db { 'asbankas':
    user    => 'asbankas',
    password => $mysql_password,
    host     => 'localhost',
    sql      => '/tmp/import.sql',
    require  => File['/tmp/import.sql']
  }

  file { "/tmp/import.sql":
    ensure => present,
    source => "puppet:///modules/mysqlcreate/import.sql",
  }

}